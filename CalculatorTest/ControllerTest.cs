﻿using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTest
{
    
    
    /// <summary>
    ///Это класс теста для ControllerTest, в котором должны
    ///находиться все модульные тесты ControllerTest
    ///</summary>
    [TestClass()]
    public class ControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///Тест для Calculate
        ///</summary>
        [TestMethod()]
        public void CalculateTest()
        {
            Controller_Accessor target = new Controller_Accessor();
            target.Calculate();
            Assert.IsNotNull(target.sb);
        }

        /// <summary>
        ///Тест для GetExpression
        ///</summary>
        [TestMethod()]
        public void GetExpressionTest()
        {
            Controller_Accessor target = new Controller_Accessor();
            string expected = string.Empty;
            string actual = target.GetExpression();
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///Тест для GetInstance
        ///</summary>
        [TestMethod()]
        public void GetInstanceTest()
        {
            Controller actual = Controller.GetInstance();
            Assert.IsNotNull(actual);
        }
    }
}
