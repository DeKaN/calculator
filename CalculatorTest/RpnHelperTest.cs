﻿using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTest
{
    
    
    /// <summary>
    ///Это класс теста для RpnHelperTest, в котором должны
    ///находиться все модульные тесты RpnHelperTest
    ///</summary>
    [TestClass()]
    public class RpnHelperTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для Calculate
        ///</summary>
        [TestMethod()]
        public void CalculateTest()
        {
            const string expression = "2 4 *";
            const double expected = 8;
            double actual = RpnHelper.Calculate(expression);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Тест для GetPriority
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Calculator.exe")]
        public void GetPriorityTest()
        {
            const char Ch = '+';
            const int Expected = 2;
            int actual = RpnHelper_Accessor.GetPriority(Ch);
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///Тест для Parse
        ///</summary>
        [TestMethod()]
        public void ParseTest()
        {
            const string Expression = "(23-20/3.14)*((89.0)+11)";
            const string Expected = "23 ( 20 3.14 / -  ( 89.0 ( * )  11 + ) )";
            string actual = RpnHelper.Parse(Expression);
            Assert.AreEqual(Expected, actual);
        }
    }
}
