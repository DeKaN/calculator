﻿namespace Calculator
{
    using System;
    using System.Globalization;
    using System.Text;
    using System.Diagnostics.Contracts;

    internal class Controller
    {
        private StringBuilder sb;
        private bool isDotAllowed, isEmpty;
        private int operationIndex;
        private static Controller instance;

        private Controller()
        {
            Reset();
        }

        public static Controller GetInstance()
        {
            return instance ?? (instance = new Controller());
        }

        public void TryToAdd(char ch)
        {
            Contract.Requires(char.IsDigit(ch) || ".+-*/".Contains(ch.ToString(CultureInfo.InvariantCulture)));
            if (isEmpty && char.IsDigit(ch))
            {
                sb.Clear();
                sb.Append(ch);
                isEmpty = false;
                return;
            }

            char lastSymbol = sb[sb.Length - 1];
            switch (ch)
            {
                case '.':
                    if (!isDotAllowed)
                    {
                        return;
                    }

                    if (!char.IsDigit(lastSymbol))
                    {
                        sb.Append(0);
                    }

                    isDotAllowed = false;
                    break;
                case '+':
                case '-':
                case '*':
                case '/':
                    if (!char.IsDigit(lastSymbol))
                    {
                        sb.Remove(sb.Length - 1, 1);
                    }

                    operationIndex = sb.Length;
                    isDotAllowed = true;
                    break;
            }

            sb.Append(ch);
            isEmpty = false;
        }

        public void Reset()
        {
            sb = new StringBuilder("0");
            isEmpty = true;
            isDotAllowed = true;
            operationIndex = 0;
        }

        internal void RemoveLastNumber()
        {
            if (sb.Length > operationIndex + 1)
            {
                sb.Remove(operationIndex + 1, sb.Length - operationIndex - 1);
            }
        }

        public string GetExpression()
        {
            return sb.ToString();
        }

        public void Calculate()
        {
            sb = new StringBuilder(RpnHelper.Calculate(RpnHelper.Parse(GetExpression())).ToString("0.####################", CultureInfo.InvariantCulture));
        }
    }
}
