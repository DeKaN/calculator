﻿namespace Calculator
{
    using System;
    using System.Globalization;
    using System.Windows.Forms;
    using System.Diagnostics.Contracts;

    public partial class MainForm : Form
    {
        private readonly Controller controller = Controller.GetInstance();

        public MainForm()
        {
            InitializeComponent();
            RefreshLabel();
        }

        private void RefreshLabel()
        {
            textBox1.Text = controller.GetExpression();
        }

        private void Button14Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null && !string.IsNullOrEmpty(b.Text))
            {
                controller.TryToAdd(b.Text[0]);
            }
            RefreshLabel();
        }

        private void Button2Click(object sender, EventArgs e)
        {
            controller.Reset();
            RefreshLabel();
        }

        private void Button1Click(object sender, EventArgs e)
        {
            controller.RemoveLastNumber();
            RefreshLabel();
        }

        private void Button19Click(object sender, EventArgs e)
        {
            try
            {
                controller.Calculate();
                RefreshLabel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
