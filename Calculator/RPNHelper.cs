﻿namespace Calculator
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using System.Diagnostics.Contracts;
	using System.Globalization;
	using System.IO;

	internal static class RpnHelper
	{
		private static int GetPriority(char ch)
		{
			switch (ch)
			{
				case '+':
                case '-':
                    return 2;
				case '*':
                case '/':
                    return 3;
				case '(':
                    return 10;
				case ')':
                    return -1;
			}

			return 0;
		}

		public static string Parse(string expression)
		{
			Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(expression));
			StringBuilder sb = new StringBuilder();
			string exp = expression.Replace("+", " + ").Replace("-", " - ").Replace("*", " * ").Replace("/", " / ");

			Stack<char> stack = new Stack<char>();
			foreach (char ch in exp)
			{
				if (char.IsDigit(ch) || ch == '.' || ch == ' ')
				{
					sb.Append(ch);
					continue;
				}

				if ((stack.Count == 0) || (GetPriority(ch) > GetPriority(stack.Peek())))
				{
					stack.Push(ch);
				}
				else
				{
					while ((stack.Count > 0) && (GetPriority(ch) <= GetPriority(stack.Peek())))
					{
						sb.Append(" ");
						char с = stack.Pop();
						if (с == '(')
							break;
						sb.Append(c).Append(" ");
					}

					if (ch != ')')
	                    stack.Push(ch);
                }
            }

            while (stack.Count > 0)
            {
                sb.Append(" ").Append(stack.Pop()).Append(" ");
            }

            return sb.Replace("  ", " ").ToString().Trim();
        }

        public static double Calculate(string expression)
        {
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(expression));
            Stack<double> stack = new Stack<double>();
            string[] arr = expression.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in arr)
            {
                if (char.IsDigit(s[0]))
                {
                    stack.Push(double.Parse(s, CultureInfo.InvariantCulture));
                }
                else
                {
                    double y = stack.Pop(), x = stack.Pop(), z;
                    switch (s)
                    {
                        case "+":
                            z = x + y;
                            break;
                        case "-":
                            z = x - y;
                            break;
                        case "*":
                            z = x * y;
                            break;
                        case "/":
                            if (y == 0) throw new ArithmeticException("Деление на ноль запрещено");
                            z = x / y;
                            break;
                        default:
                            throw new ArgumentException();
                    }

                    stack.Push(z);
                }
            }

            if (stack.Count != 1)
            {
                throw new InvalidDataException();
            }

            return stack.Pop();
        }
    }
}
